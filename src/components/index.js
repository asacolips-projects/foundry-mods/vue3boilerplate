/* eslint-disable import/prefer-default-export */
export { default as CharacterSheet } from './CharacterSheet.vue';
export { default as Editor } from './parts/Editor.vue';
export { default as Tabs } from './parts/Tabs.vue';
export { default as Tab } from './parts/Tab.vue';