export const V3BOILERPLATE = {};

// Define constants here, such as:
V3BOILERPLATE.foobar = {
  'bas': 'V3BOILERPLATE.bas',
  'bar': 'V3BOILERPLATE.bar'
};

/**
 * The set of Ability Scores used within the sytem.
 * @type {Object}
 */
 V3BOILERPLATE.abilities = {
  "str": "V3BOILERPLATE.AbilityStr",
  "dex": "V3BOILERPLATE.AbilityDex",
  "con": "V3BOILERPLATE.AbilityCon",
  "int": "V3BOILERPLATE.AbilityInt",
  "wis": "V3BOILERPLATE.AbilityWis",
  "cha": "V3BOILERPLATE.AbilityCha"
};

V3BOILERPLATE.abilityAbbreviations = {
  "str": "V3BOILERPLATE.AbilityStrAbbr",
  "dex": "V3BOILERPLATE.AbilityDexAbbr",
  "con": "V3BOILERPLATE.AbilityConAbbr",
  "int": "V3BOILERPLATE.AbilityIntAbbr",
  "wis": "V3BOILERPLATE.AbilityWisAbbr",
  "cha": "V3BOILERPLATE.AbilityChaAbbr"
};